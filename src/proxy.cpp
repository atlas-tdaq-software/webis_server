
#include "ipc/core.h"
#include "is/infodictionary.h"
#include "is/infodynany.h"
#include "oh/OHRootProvider.h"
#include "owl/time.h"

#include "TBufferFile.h"
#include "TROOT.h"

#include <boost/url.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include <sys/epoll.h>
#include <sys/wait.h>

#include <libmicrohttpd/microhttpd.h>
#include <nlohmann/json.hpp>

#include <string>
#include <vector>
#include <cstdint>
#include <map>
#include <signal.h>

OHRootProvider*
get_provider(MHD_Connection *connection,
             const std::string& partition,
             const std::string& server,
             const std::string& provider)
{
    auto key = partition + '/' + server + '.' + provider;

    auto *info = MHD_get_connection_info (connection, MHD_CONNECTION_INFO_SOCKET_CONTEXT);
    auto providers = static_cast<std::map<std::string, OHRootProvider*>*>(info->socket_context);
    auto it = providers->find(key);
    if (it != providers->end()) {
        return it->second;
    } else {
        IPCPartition p(partition);
        OHRootProvider *prov = new OHRootProvider(partition, server, provider);
        (*providers)[key] = prov;
        return prov;
    }
}


void
parse_is_url(boost::urls::segments_view segments,
             std::string& partition,
             std::string& server,
             std::string& name)
{
    // segments are / separated path segments
    // auto segments = boost::urls::parse_origin_form(url).value().segments();
    auto it = segments.begin();
    ++it;
    partition = *++it;
    if(*++it != "is") return;
    server = *++it;
    name = boost::algorithm::join(std::vector<std::string>(++it, segments.end()), "/");
}

void
parse_oh_url(boost::urls::segments_view segments,
             std::string& partition,
             std::string& server,
             std::string& provider,
             std::string& name)
{
    auto it = segments.begin();
    ++it;
    partition = *++it;
    if(*++it != "oh") return;
    auto h = *++it;
    auto dot = h.find('.');
    server = h.substr(0, dot);
    auto dot2 = h.find('.', dot + 1);
    provider = h.substr(dot + 1, dot2 - dot - 1);
    auto path_name = boost::algorithm::join(std::vector<std::string>(++it, segments.end()), "/");
    name = h.substr(dot2 + 1);
    if(name.empty()) {
        name += '/' + path_name;
    } else if (!path_name.empty()) {
        name += '/' + path_name;
    }
}

MHD_Result
get_is_object(void *cls,
              struct MHD_Connection * connection,
              boost::urls::segments_view segments,
              void **con_cls)
{
    try {
        std::string partition, server, name;
        parse_is_url(segments, partition, server, name);
        IPCPartition p(partition);
        ISInfoDictionary d(p);
        ISInfoDynAny obj;
        d.getValue(name, obj);

        nlohmann::json result;
        result[0] = name;
        result[1] = obj.type().name();
        result[2] = obj.time().str();
        result[3] = nlohmann::json::object_t();

        for(size_t a = 0; a < obj.getAttributesNumber(); a++) {
            if(obj.isAttributeArray(a)) {
                switch (obj.getAttributeType(a)) {
                case ISType::Boolean:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<bool>>(a);
                    break;
                case ISType::S8:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::int8_t>>(a);
                    break;
                case ISType::S16:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::int16_t>>(a);
                    break;
                case ISType::S32:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::int32_t>>(a);
                    break;
                case ISType::S64:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::int64_t>>(a);
                    break;
                case ISType::U8:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::uint8_t>>(a);
                    break;
                case ISType::U16:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::uint16_t>>(a);
                    break;
                case ISType::U32:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::uint32_t>>(a);
                    break;
                case ISType::U64:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::uint64_t>>(a);
                    break;
                case ISType::String:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<std::string>>(a);
                    break;
                case ISType::Float:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<float>>(a);
                    break;
                case ISType::Double:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::vector<double>>(a);
                    break;
                    // TODO
                case ISType::Date:
                case ISType::Time:
                case ISType::InfoObject:
                case ISType::Error:
                    break;
                }
            } else {
                switch (obj.getAttributeType(a)) {
                case ISType::Boolean:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<bool>(a);
                    break;
                case ISType::S8:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::int8_t>(a);
                    break;
                case ISType::S16:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::int16_t>(a);
                    break;
                case ISType::S32:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::int32_t>(a);
                    break;
                case ISType::S64:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::int64_t>(a);
                    break;
                case ISType::U8:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::uint8_t>(a);
                    break;
                case ISType::U16:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::uint16_t>(a);
                    break;
                case ISType::U32:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::uint32_t>(a);
                    break;
                case ISType::U64:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::uint64_t>(a);
                    break;
                case ISType::String:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<std::string>(a);
                    break;
                case ISType::Float:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<float>(a);
                    break;
                case ISType::Double:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<double>(a);
                    break;
                case ISType::Date:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<OWLDate>(a).str();
                    break;
                case ISType::Time:
                    result[3][obj.getAttributeDescription(a).name()] = obj.getAttributeValue<OWLTime>(a).str();
                    break;
                    // TODO
                case ISType::InfoObject:
                case ISType::Error:
                    break;
                }
            }
        }

        std::string result_str = result.dump();

        MHD_Response *response = MHD_create_response_from_buffer(result_str.size(), result_str.data(), MHD_RESPMEM_MUST_COPY);
        MHD_add_response_header(response, "content-type", "application/json");
        auto ret = MHD_queue_response(connection, 200, response);
        MHD_destroy_response(response);
        return ret;

    } catch(...) {
    }

    MHD_Response *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
    MHD_add_response_header(response, "content-type", "application/json");
    auto ret = MHD_queue_response(connection, 400, response);
    MHD_destroy_response(response);
    return ret;
}

struct post_is_state {
    std::string type;
    std::string data;
};

MHD_Result
post_is_object(void *cls,
               struct MHD_Connection * connection,
               boost::urls::segments_view segments,
               const char *upload_data,
               size_t *upload_data_size,
               void **con_cls)
{
    post_is_state *state = reinterpret_cast<post_is_state*>(*con_cls);
    if(nullptr ==state) {
        state = new post_is_state();
        *con_cls = state;
        const char *type_ptr = MHD_lookup_connection_value (connection, MHD_GET_ARGUMENT_KIND, "type");
        if(type_ptr == nullptr ) {
            // TODO: return proper error message
            return MHD_NO;
        }
        state->type = type_ptr;
        return MHD_YES;
    }

    if(upload_data != nullptr) {
        state->data.append(upload_data, *upload_data_size);
        *upload_data_size = 0;
        return MHD_YES;
    } else {
        try {
            nlohmann::json data = nlohmann::json::parse(state->data);
            std::string partition;
            std::string server;
            std::string name;
            parse_is_url(segments, partition, server, name);
            IPCPartition p(partition);
            ISInfoDynAny object(p, state->type);

            for(auto& it : data.items()) {

                auto field_name = it.key();

                switch (object.getAttributeType(field_name)) {
                case ISType::Boolean:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<bool>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<bool>());
                    }
                    break;
                case ISType::S8:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::int8_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<int8_t>());
                    }
                    break;
                case ISType::S16:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::int16_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::int16_t>());
                    }
                    break;
                case ISType::S32:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::int32_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::int32_t>());
                    }
                    break;
                case ISType::S64:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::int64_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::int64_t>());
                    }
                    break;
                case ISType::U8:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::uint8_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::uint8_t>());
                    }
                    break;
                case ISType::U16:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::uint16_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::uint16_t>());
                    }
                    break;
                case ISType::U32:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::uint32_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::uint32_t>());
                    }
                    break;
                case ISType::U64:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::uint64_t>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::uint64_t>());
                    }
                    break;
                case ISType::Float:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<float>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<float>());
                    }
                    break;
                case ISType::Double:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<double>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<double>());
                    }
                    break;
                case ISType::String:
                    if(object.isAttributeArray(field_name)) {
                        object.setAttributeValue(field_name, it.value().get<std::vector<std::string>>());
                    } else {
                        object.setAttributeValue(field_name, it.value().get<std::string>());
                    }
                    break;
                    // TODO
                case ISType::Date:
                case ISType::Time:
                case ISType::InfoObject:
                case ISType::Error:
                    break;
                }
            }

            ISInfoDictionary d(p);
            d.checkin(name, object);
            delete state;

            MHD_Response *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
            MHD_add_response_header(response, "content-type", "application/json");
            auto ret = MHD_queue_response(connection, 201, response);
            MHD_destroy_response(response);
            return ret;
        } catch(...) {

        }

        delete state;
        MHD_Response *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
        MHD_add_response_header(response, "content-type", "application/json");
        auto ret = MHD_queue_response(connection, 400, response);
        MHD_destroy_response(response);
        return ret;
    }
}

MHD_Result
post_histogram(void *cls,
               struct MHD_Connection * connection,
               boost::urls::segments_view segments,
               const char *upload_data,
               size_t *upload_data_size,
               void **con_cls)
{
    if(nullptr == *con_cls) {
        *con_cls = new std::string;
        return MHD_YES;
    }

    std::string *s = reinterpret_cast<std::string*>(*con_cls);
    if(upload_data != nullptr) {
        s->append(upload_data, *upload_data_size);
        *upload_data_size = 0;
        return MHD_YES;
    } else {
        TBufferFile buffer(TBuffer::kRead, s->size(), s->data(), false);
        if(TObject *obj = buffer.ReadObject(nullptr)) {
            try {
                std::string partition;
                std::string server;
                std::string provider;
                std::string name;
                parse_oh_url(segments, partition, server, provider, name);

                OHRootProvider *prov = get_provider(connection, partition, server, provider);
                if(TH1 *histo = dynamic_cast<TH1*>(obj)) {
                    prov->publish(*histo, name);
                } else if (TGraph *graph = dynamic_cast<TGraph*>(obj)) {
                    prov->publish(*graph, name);
                } else if (TGraph2D *graph = dynamic_cast<TGraph2D*>(obj)) {
                    prov->publish(*graph, name);
                } else if (TEfficiency *eff = dynamic_cast<TEfficiency*>(obj)) {
                    prov->publish(*eff, name);
                } else {
                    // invalid object type
                }
                delete obj;
                delete s;
                MHD_Response *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
                MHD_add_response_header(response, "content-type", "application/json");
                auto ret = MHD_queue_response(connection, 201, response);
                MHD_destroy_response(response);
                return ret;
            } catch(...) {
            }
            delete obj;
        }
        delete s;
        MHD_Response *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
        MHD_add_response_header(response, "content-type", "application/json");
        auto ret = MHD_queue_response(connection, 400, response);
        MHD_destroy_response(response);
        return ret;
    }
}

MHD_Result
callback(void *cls,
         struct MHD_Connection * connection,
         const char *url,
         const char *method,
         const char *version,
         const char *upload_data,
         size_t *upload_data_size,
         void **con_cls)
{
    auto parsed =  boost::urls::parse_origin_form(url).value();

    auto segments = parsed.segments();
    auto it = segments.begin();
    ++it; ++it;
    auto service = *++it;
    if(strcmp(method, "POST") == 0) {
        if(service == "oh") {
            return post_histogram(cls, connection, segments, upload_data, upload_data_size, con_cls);
        } else if(service == "is") {
            return post_is_object(cls, connection, segments, upload_data, upload_data_size, con_cls);
        }
    } else  if(strcmp(method, "GET") == 0 && service == "is") {
        return get_is_object(cls, connection, segments, con_cls);
    }

    MHD_Response *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
    MHD_add_response_header(response, "content-type", "application/json");
    auto ret = MHD_queue_response(connection, 404, response);
    MHD_destroy_response(response);
    return ret;
}

void connection_callback(void *cls,
                         struct MHD_Connection *connection,
                         void **socket_context,
                         enum MHD_ConnectionNotificationCode toe)
{
    if(toe == MHD_CONNECTION_NOTIFY_STARTED) {
        *socket_context = new std::map<std::string, OHRootProvider*>;
    } else if(toe == MHD_CONNECTION_NOTIFY_CLOSED) {
        if(*socket_context) {
            auto providers = static_cast<std::map<std::string, OHRootProvider*>*>(*socket_context);
            for(auto& entries : *providers) {
                delete entries.second;
            }
        }
    } // else ignore
}

void worker(unsigned short port, unsigned int threads, int flags)
{
    ROOT::EnableThreadSafety();
    TH1::AddDirectory(false);

    MHD_Daemon *daemon = MHD_start_daemon (MHD_USE_DUAL_STACK | flags,
                                           (unsigned int)port,
                                           nullptr,
                                           nullptr,
                                           callback,
                                           nullptr,
                                           MHD_OPTION_THREAD_POOL_SIZE, threads,
                                           MHD_OPTION_NOTIFY_CONNECTION, connection_callback, nullptr,
                                           MHD_OPTION_LISTENING_ADDRESS_REUSE, threads > 0,
                                           MHD_OPTION_END);
    if(daemon == nullptr) {
        std::cerr << "Cannot create web server" << std::endl;
        exit(1);
    }
    pause();
    MHD_stop_daemon(daemon);
}

std::vector<pid_t> pids;

void handler(int sig)
{
    for(auto pid : pids) {
        kill(pid, sig);
    }
}

int main(int argc, char *argv[])
{
    // Extract possible CORBA options first
    auto options = IPCCore::extractOptions(argc, argv);
    options.emplace_front("maxGIOPConnectionPerServer", "32");

    // Default options
    unsigned short port = 8080;
    unsigned int   threads = 6;
    unsigned int   forks = 0;

    int flags = MHD_USE_INTERNAL_POLLING_THREAD | MHD_USE_EPOLL;

    // Parse options
    namespace po = boost::program_options;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("threads", po::value<unsigned int>(), "number of threads")
        ("port", po::value<unsigned short>(), "port number")
        ("forks", po::value<unsigned int>(), "forks")
        ("thread-per-connection", "thread per connection mode")
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }

    if (vm.count("port")) {
        port = vm["port"].as<unsigned short>();
    }

    if(vm.count("threads")) {
        threads = vm["threads"].as<unsigned int>();
    }

    if(vm.count("forks")) {
        forks = vm["forks"].as<unsigned int>();
    }

    if(vm.count("thread-per-connection")) {
        flags = MHD_USE_THREAD_PER_CONNECTION;
        threads = 0;
    }

    if(forks == 0) {
        // single process
	IPCCore::init(options);
        worker(port, threads, flags);
    } else {
        signal(SIGTERM, handler);
        // multi process
        for(unsigned int i = 0; i < forks; i++) {
            auto pid = fork();
            if(pid == -1) {
                perror("fork");
            } else if(pid == 0) {
                // child
	        IPCCore::init(options);
                worker(port, threads, flags);
            } else {
                // parent
                pids.push_back(pid);
            }
        }

        for(unsigned int i = 0; i < forks; i++) {
            wait(nullptr);
        }
    }

}
