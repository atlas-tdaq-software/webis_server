# WebIS Server

This is the server providing the REST API to access various
ATLAS TDAQ Online services, including

  * Information Service
  * Online Histograms
  * OKS Database (for running partitions)
  * Error Reporting Service

## Running the Server

Setup the TDAQ release you want to use and

    cm_setup tdaq-09-05-00
    webis_server

You can now reach it under `http://localhost:8080/info/current`

### Server Options

To change the port number for the server:

    webis_server --port=5000

To choose a random port and have it written to a file:

    webis_server --port=0 --port-file=/tmp/port-number

To use a Unix socket instead of TCP:

    webis_server --unix /tmp/www

To make the static web pages associated with the service
available for testing:

    git clone https://gitlab.cern.ch/atlas-tdaq-software/webis.git /path/to/webis
    webis_server --www=/path/to/webis

They will be visible under:

    http://localhost:<port>/tdaq/web_is

i.e. the same relative location as on `atlasop.cern.ch`.

To change the default log level (which is the Python default of WARNING):

    webis_server --loglevel=INFO

If you pass a X.509 certificate or private key file, the server will
use TLS. In addition HTTP/2 support will be available.

    webis_server --cert=./cert.pem

This assumes that the `cert.pem` file has both the certificate and
the private key. If the latter is in the separate file, pass it
via the `--key=` option:

    webis_server --cert=./cert.pem --key=./key.pem

To create these:

    openssl genrsa -out key.pem
    openssl x509 -new -key key.pem -out cert.pem -subj /CN=myself

By default the server will refuse any modifying operations. To enable them:

    webis_server --writeable

This enables `POST` and `DELETE` methods on IS objects and OH histograms.
