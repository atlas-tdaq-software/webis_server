# webis_server

Added the C++ based `webproxy` executable. This implements a subset
of the full `webis_server` executable, namely:

  * Read and write IS objects in the 'compact' format
  * Publish ROOT histograms in binary format

This is a Phase II prototype for initial measurements when used with
HLT and global monitoring jobs.

## tdaq-11-03-00

The JSON based API no longer HTML escapes the metadata for the object. This is
unnecessary since the JSON invalid characters are different from HTML and the
JSON encode does its own escaping anyway.

The one mostly affected are histograms requested as raw IS value in JSON - however,
this should be a basically non-existant use case. The type names e.g. change from
`HistogramData&lt;int&gt;` to `HistogramData<int>`.

## tdaq-11-00-00

The JSON based API now returns a JSON formatted response in case of an error. It will
contain an object with two keys:

```json
{
    "error" : "error_value",
    "error_description: "human readable error message"
}
```

The `error` value is one of a few defined strings, containing no whitespace. It can
be used to check for certain error types (e.g. `invalid_partition`). The
`error_description` is a human readable string that can be presented to the user.
