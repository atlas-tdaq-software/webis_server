FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7 AS builder

ARG TDAQ_VERSION=tdaq-09-04-00

RUN mkdir -p /work/build
RUN cp /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake/templates/CMakeLists.txt /work/
RUN git clone https://gitlab.cern.ch/atlas-tdaq-software/ispy.git /work/ispy
WORKDIR /work/build
RUN source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh $TDAQ_VERSION && cmake .. && make install

FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7

ENV TDAQ_VERSION=tdaq-09-04-00

ENV WEBROOT=/webroot
ENV LOGFILE=/dev/stdout
ENV EXTRA_ARGS=

COPY --from=builder /work/installed /work/installed
RUN  mkdir -p /webroot && git clone -b testbed https://gitlab.cern.ch/atlas-tdaq-software/webis.git /webroot
COPY *.py *.sh /webis_server

CMD source /work/installed/setup.sh && /webis_server/webis_service.sh ${EXTRA_ARGS}

EXPOSE 5000
