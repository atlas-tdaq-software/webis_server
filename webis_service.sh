#!/bin/bash
# The version requested when no release is setup
TDAQ_VERSION=${TDAQ_VERSION:=tdaq-10-00-00}

if [ -z "$TDAQ_INST_PATH" ]; then
    if [ -f /det/tdaq/scripts/setup_TDAQ_${TDAQ_VERSION}.sh ]; then
        source /det/tdaq/scripts/setup_TDAQ_${TDAQ_VERSION}.sh
    elif [ -f /sw/tdaq/setup/setup_${TDAQ_VERSION}.sh ]; then
        source /sw/tdaq/setup/setup_${TDAQ_VERSION}.sh 
    elif [ -f /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh ]; then
        source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh ${TDAQ_VERSION}
    else
        echo "Please setup the TDAQ release first, cannot find it in standard locations"
        exit 1
    fi
fi

LOGFILE=${LOGFILE:=/logs/${CMTRELEASE}/webis/webis.${HOSTNAME}}
PORT=${PORT:=5000}
WEBROOT=${WEBROOT:-$(dirname $0)/webis}

mkdir -p $(dirname ${LOGFILE})

# Used for special environments like paas.cern.ch
if [ -n "${IPC_INIT_REF}" ]; then
  export TDAQ_IPC_INIT_REF=${IPC_INIT_REF}
fi

unset DISPLAY
[ -f ${LOGFILE} ] && mv ${LOGFILE} ${LOGFILE}.$$ 2> /dev/null

exec python3 -m webis_server.server --port=${PORT} --www=${WEBROOT} $* > ${LOGFILE} 2> ${LOGFILE}
