
import logging
import string
import cgi
import html
import json
import urllib.parse
import uuid

from twisted.internet import reactor, threads, task
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

import ipc
import emon

logger = logging.getLogger(__name__)
subscriptions = {}

class EMONData(Resource):
    """
    Retrieve one event from EMON
    """
    def __init__(self, partition, subscription):
        Resource.__init__(self)
        self.partition    = partition
        self.subscription = subscription

    def render_DELETE(self, request):
        try:
            del subscriptions[self.subscription]
            return b''
        except:
            logger.info("unsubscribe", exc_info=True)
            request.setResponseCode(404)
            return b'<html>No such subscription</html>'

    def render_GET(self, request):
        logger.info("event request for %s from %s", self.subscription, request.getClientAddress())
        reactor.callInThread(self.do, request)
        return NOT_DONE_YET

    def do(self, request):
        p = ipc.IPCPartition(self.partition)
        request.setResponseCode(404)

        if not self.subscription in subscriptions:
            request.write(b'<html>No such subscription</html>')
            request.finish()
            return

        if not p.isValid():
            request.write(bytes('<html>No such partition: %s</html>' % self.partition,'utf-8'))
            request.finish()
            return

        try:
            event_it = subscriptions[self.subscription]
            event = event_it.nextEvent(10000)
            request.setHeader('content-type', 'application/octet-stream')
            request.setHeader('content-length', str(len(event) * 4))
            request.setResponseCode(200)
            for data in event:
                request.write(data.to_bytes(4, 'little'))
        except:
            logger.info("error getting event", exc_info=True)
            request.write(b'<html>No event</html>')

        request.finish()
        return

class EMONResource(Resource):
    """
    Subscription to EMON
    """

    def __init__(self, partition):
        Resource.__init__(self)
        self.partition = partition

    def do_POST(self, request):
        try:
            req = json.load(request.content)

            logger.info("got request: %s", req)

            lvl1_trigger_type = emon.L1TriggerType(req.get('lvl1_trigger_type', 0),
                                                   not 'lvl1_trigger_type' in req)
            status_word       = emon.StatusWord(req.get('status_word', 0),
                                                not 'status_word' in req)
            lvl1_trigger_bits = emon.L1TriggerBits(req.get('lvl1_trigger_bits', []),
                                                   getattr(emon.Logic, req.get('lvl1_trigger_logic', "IGNORE")),
                                                   getattr(emon.Origin, req.get('lvl1_trigger_origin', "AFTER_VETO")))
            stream_tags       = emon.StreamTags(req.get('stream_tag', ''),
                                                req.get('stream_names', []),
                                                getattr(emon.Logic, req.get('stream_logic', "IGNORE")))
            if 'sampler_count' in req:
                address           = emon.SamplingAddress(req['sampler_type'],
                                                         req.get('sampler_count', 0))
            else:
                address           = emon.SamplingAddress(req['sampler_type'],
                                                         req.get('sampler_keys', []))

            event_it = emon.EventIterator(ipc.IPCPartition(self.partition),
                                          address,
                                          emon.SelectionCriteria(lvl1_trigger_type,
                                                                 lvl1_trigger_bits,
                                                                 stream_tags,
                                                                 status_word))
            gid = str(uuid.uuid4())
            subscriptions[gid] = event_it
            logger.info("new subscription: %s from %s", gid, request.getClientAddress())
            request.setResponseCode(201)
            request.write(f'{{ "id" : "{gid}" }}'.encode('utf-8'))
            request.finish()
        except:
            logger.info("EMON error", exc_info=True)
            request.write(b'{ "error" : "exception" }')
            request.finish()

    def render_POST(self, request):
        p = ipc.IPCPartition(self.partition)

        request.setResponseCode(400)
        request.setHeader('content-type', 'application/json')

        if not p.isValid():
            return b'{ "error": "No such partition"}'

        if request.getHeader('content-type') != 'application/json':
            return b'{ "error": "JSON expected" }'

        reactor.callInThread(self.do_POST, request)
        return NOT_DONE_YET


    def getChild(self, subscription, request):
        if subscription == b'':
            return self
        return EMONData(self.partition, subscription.decode('utf-8'))

