
import json
import cgi
import html
from ispy import *

def valueToXML(value, is_info, attr):
    if is_info:
        return toXML('', value, attr)
    else:
        return html.escape(str(value))

def get_format(fmt):
    if fmt == '0x{:x}': return ' fmt="h"'
    if fmt == '0o{:o}': return ' fmt="o"'
    return ''

def toXML(name, obj, attr):
    result = '<obj name="'+ html.escape(name)  + '" type="'+html.escape(obj.type().name()) + '" time="'+ str(obj.time())+'">\n'
    for i,n in enumerate(obj.keys()):
        if attr and n not in attr:
            continue
        values = obj[i]
        result += ' <attr name="'+ n +'" descr="' + html.escape(obj.getAttributeDescription(i).description()) +'" type="' +  html.escape(obj.getAttributeDescription(i).typeName()) + '" range="' +  html.escape(obj.getAttributeDescription(i).range()) + '"' + get_format(obj.getAttributeDescription(i).format())
        if obj.isAttributeArray(i):
          try:
            result += ' multi="1">\n'
            for v in values:
                result += '<v>' + valueToXML(v, obj.getAttributeType(i) == Basic.ISInfo, attr) + '</v>'
          except Exception as ex:
            # big mystery
            pass
        else:
            result += '>\n'
            result += '<v>' + valueToXML(values, obj.getAttributeType(i) == Basic.ISInfo, attr) + '</v>'
        result += '</attr>\n'
    result += '</obj>\n'
    return result

def get_format_json(fmt):
    if fmt == '0x{:x}': return '"fmt":"h", '
    if fmt == '0o{:o}': return '"fmt":"o", '
    return ''

def valueToJSON(value, attr_type, attr):
    if attr_type == Basic.ISInfo:
        return toJSON('', value, attr)
    elif attr_type in [ Basic.String, Basic.Time, Basic.Date ]:
        return json.dumps(str(value))
    else:
        return json.dumps(value)

def toJSON(name, obj, attr):
    result = '[ "%s", "%s", "%s", ' % ( name, obj.type().name(), str(obj.time()) )
    add_comma = False
    for i,n in enumerate(obj.keys()):

        if attr and  n not in attr:
            continue

        if add_comma:
            result += ','

        descr = obj.getAttributeDescription(i)

        result += '''{ "name": "%s", "descr": "%s", "type": "%s", "range": "%s", %s "value": ''' % ( n,
             descr.description(),
             descr.typeName(),
             descr.range(),
             get_format_json(descr.format())
             )

        if obj.isAttributeArray(i):
            result += '['
            result += ','.join(valueToJSON(v, obj.getAttributeType(i), attr) for v in obj[i])
            result += ']'
        else:
            result += valueToJSON(obj[i], obj.getAttributeType(i), attr)
        result += '}'
        add_comma = True

    result += ']'
    return result

def toPython(name, obj):
    d = {}
    for i, n in enumerate(obj.keys()):
        d[n] = obj[i]
    return [ name, obj.type().name(), str(obj.time()), d]
