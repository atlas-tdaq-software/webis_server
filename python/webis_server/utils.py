import logging
import json

logger = logging.getLogger(__name__)

def write(request, message = None, finish = False, code = None, headers = None):
    """
    A helper routine to be calledt to write the given message
    back to the client, but execute in the reactor thread:

    reactor.callFromThread(request, message, ...)
    """
    if not headers:
        headers = {}

    try:
        if code:
            request.setResponseCode(code)
        for key, val in headers.items():
            request.setHeader(key, val)
        if message:
            request.write(message)
        if finish:
            request.finish()
    except:
        logger.error("Failed to write to %s", request.getClientAddress(), exc_info=True)

INVALID_PARTITION = 'invalid_partition'
INVALID_SERVER    = 'invalid_server'
INVALID_OBJECT    = 'invalid_object'
PERMISSION_DENIED = 'permission_denied'
OPERATION_FAILED  = 'operation_failed'

def error_msg(err, msg, use_json = True):
    if use_json:
        return bytes(json.dumps({
            "error" : err,
            "error_description": msg
            }), 'utf-8')
    else:
        return bytes(f'<html><body><h1>Error</h1><p>{msg}</body></html>', 'utf-8')
