from past.builtins import cmp
from builtins import bytes
import string
import cgi
import cgi
import json
import html
import time
import threading

from twisted.internet import reactor, threads
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

import ipc
import config

from .utils import write

lock = threading.Lock()
cache = {}

def get_db(partition):
    global cache
    with lock:
        entry = cache.get(partition)
        if entry and time.time() - entry[0] < 180:
            return entry[1]
        if entry:
          del cache[partition]
        if partition != 'initial':
            c = config.Configuration(f'rdbconfig:RDB@{partition}')
        else:
            c = config.Configuration(f'rdbconfig:RDB_INITIAL@{partition}')
        cache[partition] = (time.time(), c)
    return c

class OKSObjectResource(Resource):

    isLeaf = True

    def __init__(self, partition, class_name, name):
        Resource.__init__(self)
        self.partition = partition
        self.class_name = class_name
        self.name = name

    def doJSON(self, request, obj, conf, formats):
        attrs = conf.attributes(obj.class_name(), True)
        rels  = conf.relations(obj.class_name(), True)

        request.setHeader('content-type', 'application/json')

        result = [ obj.UID(), obj.class_name(), [], [] ]

        for a in attrs:
            value = obj[a]
            if value == None:
                value = attrs[a]['init-value']
                if value == None: value = ''

            result[2].append({ 'name': a, 'type': attrs[a]['type'], 'descr': html.escape(attrs[a]['description']), 'value': value })

        for a in rels:
            target = obj[a]
            if isinstance(target, list):
                target = [ (t.UID(), t.class_name()) for t in target ]
            elif not target is None:
                target = (target.UID(), target.class_name())
            else:
                target = []

            result[3].append({ 'name': a, 'type': rels[a]['type'], 'descr':  html.escape(rels[a]['description']), 'target': target })

        if b'compact' in formats:
            result[2] = { a['name']: a['value'] for a in result[2] }
            result[3] = { a['name']: a['target'] for a in result[3] }

        reactor.callFromThread(write, request, bytes(json.dumps(result),'utf-8'), finish=True)

    def doXML(self, request, obj, conf):
        attrs = conf.attributes(obj.class_name(), True)
        rels  = conf.relations(obj.class_name(), True)

        result = bytes('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="../../../../../tdaq/web_is/xsl/default.xsl"?><oks-data release="current" partition="%s">' % self.partition,'utf-8')
        result += bytes('<obj name="%s" type="%s">' % ( obj.UID(), obj.class_name() ),'utf-8')

        for a in attrs:
            value = obj[a]
            if value != None:
                if type(value) != list:
                    value = [value]
            else:
                value = attrs[a]['init-value']
                if value == None: value = []
                if type(value) != list: value = [value]

            result += bytes('<attr name="%s" type="%s" multi="%s">' % ( a, attrs[a]['type'], attrs[a]['multivalue']),'utf-8')
            result += bytes('<descr>' + html.escape(attrs[a]['description']) + '</descr>','utf-8')
            for v in value:
                result += bytes('<val>%s</val>' %  html.escape(str(v)),'utf-8')
            result += b'</attr>'

        for a in rels:
            target = obj[a]
            if target:
                if type(target) != list:
                    target = [ target ]
            else:
                target = []
            result += bytes('<rel name="%s" type="%s" multi="%s">' % ( a, rels[a]['type'], rels[a]['multivalue']),'utf-8')
            result += bytes('<descr>' + html.escape(rels[a]['description']) + '</descr>','utf-8')
            for t in target:
                result += bytes('<target name="%s" type="%s"/>' % ( t.UID(), t.class_name() ),'utf-8')
            result += bytes('</rel>','utf-8')

        result += b'</obj>\n</oks-data>'
        reactor.callFromThread(write, request, result, finish=True, headers={ 'content-type': 'application/xml' })

    def do(self, request):

        try :
            c = get_db(self.partition)
        except:
            reactor.callFromThread(write, request, bytes('<html>No such partition: %s</html>' % self.partition,'utf-8'), finish=True)
            return

        try:
            obj = c.get_obj(self.class_name, self.name)
        except:
            reactor.callFromThread(write, request, bytes('<html>No such object: %s@%s</html>' % (self.name, self.class_name),'utf-8'), finish=True)
            return

        formats = request.args.get(b'format', [])
        if b'json' in formats or b'compact' in formats:
            self.doJSON(request,obj, c, formats)
        else:
            self.doXML(request, obj, c)

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

class OKSNamesResource(Resource):

    def __init__(self, partition, class_name):
        Resource.__init__(self)
        self.partition  = partition
        self.class_name = class_name

    def do(self, request):
        try :
            c = get_db(self.partition)
        except:
            reactor.callFromThread(write, request, bytes('<html>No such partition: %s</html>' % self.partition,'utf-8'), finish=True)
            return

        objs = list(c.get_objs(self.class_name))
        objs.sort(key=lambda x: x.UID().upper())

        if b'json' in request.args.get(b'format',[]):
            reactor.callFromThread(write, request, bytes(json.dumps([ obj.UID() for obj in objs]),'utf-8'), finish=True, headers={'content-type': 'application/json'})
        else:
            reactor.callFromThread(write, request, bytes("""<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="../../../../tdaq/web_is/xsl/default.xsl"?>
<oks-objects release="current" partition="%s" type="%s">
%s
</oks-objects>
""" % ( self.partition, self.class_name, '\n'.join([ '<obj name="%s" type="%s"/>' % (x.UID(), x.class_name()) for x in objs ]) ),'utf-8'), finish=True, headers={'content-type': 'application/xml'})

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

    def getChild(self, name, request):
        if name == b'': return self
        return OKSObjectResource(self.partition, self.class_name, name.decode())

class OKSClassResource(Resource):

    def __init__(self, partition):
        Resource.__init__(self)
        self.partition = partition

    def do(self, request):
        try:
            c = get_db(self.partition)
        except:
            reactor.callFromThread(write, request, bytes('<html>No such partition: %s</html>' % self.partition,'utf-8'), finish=True)
            return

        cl = list(c.classes())
        cl.sort(key=lambda x: x.upper())

        if b'json' in request.args.get(b'format',[]):
            reactor.callFromThread(write, request, bytes(json.dumps(cl),'utf-8'), finish=True, headers={ 'content-type': 'application/json'})
        else:
            reactor.callFromThread(write, request, bytes("""<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="../../../tdaq/web_is/xsl/default.xsl"?>
<oks release="current" partition="%s">
%s
</oks>
""" % (self.partition, '\n'.join([ '<oks-class name="%s"/>' % name for name in cl ])) ,'utf-8'), finish=True, headers={ 'content-type': 'application/xml' })

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

    def getChild(self, class_name, request):
        if class_name == b'': return self
        return OKSNamesResource(self.partition, class_name.decode())
