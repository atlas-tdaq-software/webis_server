import logging
import string
import cgi
import html
import json

from twisted.internet import reactor, threads, task
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

import ipc
from ispy import *
from .is_format import *

from .utils import *

logger = logging.getLogger(__name__)

# The list of active ISSubscription(s)
# This is needed to keep the objects alive until the client
# closes the connection.
subscriptions = []

class ISSubscription(ISInfoReceiver):

    def __init__(self, partition, server, name, request):
        ISInfoReceiver.__init__(self, IPCPartition(partition), True)

        logger.info("New client %s for partition %s: %s %s", request.getClientAddress(), partition, server, name)

        self.partition = partition
        self.server    = server
        self.name      = name
        self.request   = request
        self.formats   = request.args.get(b'format',[])

        reason = []
        listening = request.args.get(b'listen', ['update'])
        if b'create' in listening: reason.append(Reason.Created)
        if b'update' in listening: reason.append(Reason.Updated)
        if b'delete' in listening: reason.append(Reason.Deleted)
        if b'subscribe' in listening: reason.append(Reason.Subscribed)

        send_data = b'json' in self.formats or b'compact' in self.formats
        multiple  = b'multiple' in request.args
        istype = request.args.get(b'type', None)
        if istype:
            istype = istype[0].decode()
            # type implies multiple
            multiple = True

        try:
            # Based on all the arguments, determine the correct type of subscription
            # 'send_data' determines if we call subscribe_info() or subscribe_event()
            self.criteria = None
            if istype:
                # name regex plus type
                self.criteria = ISCriteria(name.removeprefix(server + '.'), ISInfoDynAny(IPCPartition(self.partition), istype).type(), Logic.AND)
            elif multiple:
                # name regex only
                self.criteria = ISCriteria(name.removeprefix(server + '.'))

            if multiple:
                if send_data:
                    self.scheduleSubscription_info(server, self.callback, self.criteria, reason)
                else:
                    self.scheduleSubscription_event(server, self.callback, self.criteria, reason)
            else:
                if send_data:
                    self.scheduleSubscription_info(name, self.callback, reason)
                else:
                    self.scheduleSubscription_event(name, self.callback, reason)

            self.keep_alive = task.LoopingCall(self.ping)
            self.keep_alive.start(15)

            deferred = request.notifyFinish()
            deferred.addBoth(self.remove_client)

        except:
            logger.info("IS Subscription failed", exc_info=True)

    def callback(self, cb):

        logger.debug("Handling IS callback from %s %s", self.server, self.name)

        try:
            # default is only metadata
            # depending on self.formats choose another output type
            payload = json.dumps([ cb.name(), cb.type().name(), str(cb.time()) ])
            if isinstance(cb, ISCallbackInfo):
                obj = ISInfoDynAny()
                cb.value(obj)
                if b'compact' in self.formats:
                    payload = json.dumps(toPython(cb.name(), obj), default=str)
                elif b'json' in self.formats:
                    payload = toJSON(cb.name(), obj, [])
            reactor.callFromThread(write, self.request, bytes(f'event: {str(cb.reason()).lower()}\ndata: {payload}\n\n', 'utf-8'))
        except:
            logger.error("Callback handler failed for  %s", self.request.getClientAddress(), exc_info=True)

    def remove_client(self, failure):
        try:
            logger.info("Removing subscription for %s partition %s: %s %s", self.request.getClientAddress(), self.partition, self.server, self.name)

            self.keep_alive.stop()
            reactor.callInThread(self.do_unsubscribe)
        except:
            logger.error("Removing subscription failed", exc_info=True)

    def do_unsubscribe(self):
        try:
            if self.criteria:
                self.unsubscribe(self.server, self.criteria, True)
            else:
                self.unsubscribe(self.name, True)
            # reactor.callLater(30, lambda : subscriptions.remove(self))
            subscriptions.remove(self)
        except:
            logger.error("CORBA unsubscribe failed")

    def ping(self):
        try:
            # called from proper thread
            self.request.write(b':keep-alive\n\n')
        except:
            logger.error("Error in ping", exc_info=True)

class ISObjectResource(Resource):
    """
    Return an IS object.
    """

    isLeaf = True

    def __init__(self, partition, server, name, writable):
        Resource.__init__(self)
        self.partition = partition
        self.server    = server
        self.name      = name
        self.writable  = writable

    def do(self, request):
        p = ipc.IPCPartition(self.partition)

        formats = request.args.get(b'format', [])
        use_json = (b'json' in formats) or (b'compact' in formats)

        if not p.isValid():
            reactor.callFromThread(write, request,
                                   error_msg(INVALID_PARTITION, f'No such partition: {self.partition}', use_json),
                                   True, 404, headers = { 'content-type' : 'application/json' } or None)
            return

        if (b'listen' in request.args or
            request.getHeader("Accept") == "text/event-stream"
            ):
            request.setHeader('content-type', 'text/event-stream')
            request.setHeader('connection', 'keep-alive')
            request.setHeader('cache-Control', 'no-cache, must-revalidate')
            # reactor.callFromThread(write, request, b': subscribed\n\n')
            subscriptions.append(ISSubscription(self.partition, self.server, self.name, request))

            return

        attr = []
        for a in request.args.get(b'attr',[]):
            attr.extend(a.decode().split(','))

        try:
            obj = ISInfoDynAny()
            display = ''
            if b'multiple' in request.args:
                if b'json' in formats:
                    display = []

                istype   = request.args.get(b'type', None)
                if istype is not None:
                    criteria = ISCriteria(self.name.split('.',1)[1], ISInfoDynAny(p, istype[0].decode()).type(), Logic.AND)
                else:
                    criteria = ISCriteria(self.name.split('.',1)[1])

                it = ISInfoIterator(p, self.name.split('.')[0], criteria)

                while it.next():
                    it.value(obj)
                    if b'json' in formats:
                        display.append(toJSON(it.name(), obj, attr))
                    elif b'compact' in formats:
                        display.append(json.dumps(toPython(it.name(), obj), default=str))
                    else:
                        display += toXML(it.name(), obj, attr)
                if b'json' in formats or b'compact' in formats:
                    display = '[\n' + string.join(display,',\n') + '\n]'
            elif b'tags' in request.args:
                d = ISInfoDictionary(p)
                tags = [ (t[0], t[1].c_time()) for t in d.getTags(self.name) ]
                if b'json' or b'compact' in formats:
                    display = json.dumps(tags, default=str)
                else:
                    display = ''
            else:
                d = ISInfoDictionary(p)
                d.getValue(self.name, obj)

                if b'json' in formats:
                    display = toJSON(self.name, obj, attr)
                elif b'compact' in formats:
                    display = json.dumps(toPython(self.name, obj), default=str)
                else:
                    display = toXML(self.name, obj, attr)

        except Exception as e:
            logger.error('get object failed: %s', self.name, exc_info=True)
            reactor.callFromThread(write, request, error_msg(INVALID_OBJECT, f'No such object: {self.name}', use_json),
                                   True, code=404,
                                   headers = use_json and { 'content-type' : 'application/json' } or None)
            return

        if b'json' in formats or b'compact' in formats:
            reactor.callFromThread(write, request,  bytes(display,'utf-8'), finish=True,
                                   headers = {
                                       'content-type': 'application/json'
                                   })
        else:
            reactor.callFromThread(write, request,
                                   bytes('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="../../../../../%stdaq/web_is/xsl/default.xsl"?><objects release="current" partition="%s" server="%s" tag="%s">%s</objects>' % ( self.name.count('/') * '../', self.partition, self.server, '', display ),'utf-8'),
                                   finish=True,
                                   headers = {
                                       'content-type': 'application/xml'
                                   })

    def render_GET(self, request):
        self.request = request
        reactor.callInThread(self.do, request)
        return NOT_DONE_YET

    def do_DELETE(self, request):
        try:
            d = ISInfoDictionary(IPCPartition(self.partition))
            d.remove(self.name)
            reactor.callFromThread(write, request, finish=True)
        except:
            logger.info("Delete %s failed", self.name, exc_info=True)
            reactor.callFromThread(write, request, error_msg(OPERATION_FAILED, f'Deleting {self.name} failed', True),
                                   finish=True, code=400)

    def render_DELETE(self, request):
        request.setHeader('content-type', 'application/json')
        if not self.writable:
            request.setResponseCode(403)
            return error_msg(PERMISSION_DENIED, 'DELETE not allowed on this server', True)
        reactor.callInThread(self.do_DELETE, request)
        return NOT_DONE_YET

    def do_POST(self, request):
        try:
            req = json.load(request.content)
            obj = ISObject(IPCPartition(self.partition), self.name, request.args.get(b'type')[0].decode())
            for k, v in req.items():
                if obj.getAttributeType(k) == Basic.Date:
                    v = OWLDate(v)
                elif obj.getAttributeType(k) == Basic.Time:
                    v = OWLTime(v)
                obj[k] = v
            obj.checkin()
            reactor.callFromThread(write, request, b'{ "result": "ok" }', True, code=201)
        except:
            logger.info('post', exc_info=True)
            reactor.callFromThread(write, request, error_msg(OPERATION_FAILED, f'Checkin failed for {self.name}', True), True, code=400)

    def render_POST(self, request):
        request.setHeader('content-type', 'application/json')
        if not self.writable:
            request.setResponseCode(403)
            return error_msg(PERMISSION_DENIED, 'POST not allowed on this server', True)

        reactor.callInThread(self.do_POST, request)
        return NOT_DONE_YET


class ISNamesResource(Resource):
    """
    Return the list of object in an IS server.
    """

    def __init__(self, partition, server, writable):
        Resource.__init__(self)
        self.partition = partition
        self.server    = server
        self.writable  = writable

    def do(self, request):

        use_json = b'json' in request.args.get(b'format', [])

        p = ipc.IPCPartition(self.partition)

        if not p.isValid():
            reactor.callFromThread(write,
                                   request,
                                   error_msg(INVALID_PARTITION, f'No such partition: {self.partition}', use_json),
                                   finish=True,
                                   code=404,
                                   headers = use_json and { 'content-type': 'application/json' } or None)
            return

        regex = request.args.get(b'regex',[b'.*'])[0].decode()
        type_name = request.args.get(b'type',[])
        if type_name: type_name = [ t.decode() for t in type_name ]
        get_tags = request.args.get(b'tags')

        try:
            it = ISInfoIterator(p, self.server, ISCriteria(regex))
        except:
            reactor.callFromThread(write,
                                   request,
                                   error_msg(INVALID_SERVER, f'No such IS server: {self.server}', use_json),
                                   finish=True,
                                   code=404,
                                   headers = use_json and { 'content-type': 'application/json' } or None)
            return

        if use_json:
            result = b'['
            add_comma = ''
            while it.next():
                if not type_name or (it.type().name() in type_name):
                    result += bytes(add_comma + json.dumps({
                        'name': html.escape(it.name()),
                        'type': html.escape(it.type().name()),
                        'time': str(it.time())
                    }),'utf-8')
                    add_comma = ','
            result += b']'
            reactor.callFromThread(write, request, result, finish=True,
                                   headers = {
                                       'content-type': 'application/json'
                                   })
        else:
            request.setHeader('content-type', 'application/xml')

            request.write(bytes('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="../../../../tdaq/web_is/xsl/default.xsl"?><names release="current" partition="%s" server="%s" count="%d">' % (self.partition, self.server, it.entries()),'utf-8'))

            while it.next():
                if not type_name or (it.type().name() in type_name):
                    request.write(bytes('<obj name="%s" type="%s" time="%s"/>\n' % ( html.escape(it.name()), html.escape(it.type().name()), str(it.time())),'utf-8'))
            request.write(b"</names>")

            request.finish()

    def render_GET(self, request):
        reactor.callInThread(self.do, request)
        return NOT_DONE_YET

    def getChild(self, name, request):
        if name == b'': return self
        name = name.decode()
        if request.postpath:
            name += '/' + '/'.join([ p.decode() for p in request.postpath ])
        return ISObjectResource(self.partition, self.server, name, self.writable)

class ISServersResource(Resource):
    """
    Return the list of IS servers in a partition.
    """

    def __init__(self, partition, writable):
        Resource.__init__(self)
        self.partition = partition
        self.writable  = writable

    def do(self, request):
        p = ipc.IPCPartition(self.partition)

        use_json = b'json' in request.args.get(b'format', [])

        if not p.isValid():
            reactor.callFromThread(write, request,
                                   error_msg(INVALID_PARTITION, f'No such partition: {self.partition}', use_json),
                                   finish=True, code=404,
                                   headers = use_json and { 'content-type': 'application/json' } or None)
            return

        servers = [ i for i in ISServerIterator(p) ]
        servers.sort(key=lambda x: x[0])

        if use_json:
            reactor.callFromThread(write,
                                   request,
                                   bytes(json.dumps([
                                       { 'name': s[0],
                                         'owner': s[1],
                                         'host': s[2],
                                         'pid': s[3],
                                         'time': str(s[4]),
                                         'ctime': '%d' % s[4].c_time() } for s in servers
                                   ]),'utf-8'),
                                   finish=True,
                                   headers = {
                                       'content-type': 'application/json'
                                   })
        else:
            reactor.callFromThread(write,
                                   request,
                                   bytes("""<?xml version="1.0" encoding="UTF-8"?>
                                   <?xml-stylesheet type="text/xsl" href="../../../tdaq/web_is/xsl/default.xsl"?>
                                   <servers release="current" partition="%s">
                                   %s
                                   </servers>
                                   """ % ( self.partition, '\n'.join([ '<server name="%s" owner="%s" host="%s" pid="%d" time="%s" ctime="%d"/>' % (s[0], s[1], s[2], s[3], s[4], s[4].c_time()) for s in servers ])),'utf-8'),
                                   finish=True,
                                   headers = {
                                       'content-type': 'application/xml'
                                   })

    def render_GET(self, request):
        reactor.callInThread(self.do, request)
        return NOT_DONE_YET

    def getChild(self, server, request):
        if server == '': return self
        return ISNamesResource(self.partition, server.decode(), self.writable)
