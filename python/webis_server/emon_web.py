from builtins import bytes
import logging
import string
import cgi
import html
import json

from twisted.internet import reactor, threads
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

import ipc
from ispy import IS, ISInfoStream, ISCriteria, ISInfoDynAny
import libispy
import emon
from webemon import emon_helper

def construct_xml(dictionary,element_type):
    """
    Construct xml string for monitor/sampler
        dictionary: python dictionary for monitor/sampler
        element_type: monitor or sampler
    """
    string = '<{} '.format(element_type)
    for key in dictionary:
        value = dictionary[key]
        string += '{}="{}" '.format(key,value)
    string += '/>'
    return string

def get_samplers(partition,samplerType=None):
    """
    Return list of dictionaries of samplers
    """
    p = ipc.IPCPartition(partition)
    sampler_list = emon.getEventSamplers(p) # List with strings as "samplerType:samplerName"
    sampler_name_list = [s.split(":",1)[1] for s in sampler_list]

    stream = ISInfoStream(p, 'Monitoring', ISCriteria('/Sampler/.*',IS.SamplerInfo(p, '').type(),libispy.Logic.AND), False, 1)

    samplers = []
    while not stream.eof():
        name = stream.name()
        sampler = ISInfoDynAny()
        stream.get(sampler)

        sampler_dict =  dict(sampler)

        # Discard samplers not found by the getEventSamplers method
        if sampler_dict['m_name'] not in sampler_name_list:
            continue

        # Discard samplers with the wrong type, if type option is chosen
        if sampler_dict['m_type'] != samplerType and samplerType != None:
            continue

        sampler_dict['time'] = bytes(sampler.time())
        sampler_dict['ctime'] = sampler.time().c_time()
        sampler_dict['name'] = name.replace("Monitoring./Sampler/","")

        samplers.append(sampler_dict)

    return samplers

class EMONEventResource(Resource):
    """
    Return an EMON event.
    """

    isLeaf = True

    def __init__(self, partition ,samplerType, name):
        Resource.__init__(self)
        self.partition = partition
        self.samplerType = samplerType
        self.name = name

    def do(self, request):
        p = ipc.IPCPartition(self.partition)

        if not p.isValid():
            request.setResponseCode(404)
            request.write('<html>No such partition: %s</html>' % self.partition)
            request.finish()
            return
        try:
            request.setHeader('Content-Type', 'application/xml')
            request.write(emon_helper.getEvent(self.partition, self.samplerType, self.name, [ request.args[a][0] for a in request.args ]))
            request.finish()
        except Exception as ex:
            request.setResponseCode(404)
            request.setHeader('Content-Type', 'text/plain')
            request.write(ex.message)
            request.finish()

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

class EMONSamplerTypeResource(Resource):
    """
    Intermediate path to event. Could return
    list of samplers of this type ?
    """

    def __init__(self, partition, samplerType):
        Resource.__init__(self)
        self.partition = partition
        self.samplerType = samplerType

    def getChild(self, name, request):
        if name == b'': return self
        return EMONEventResource(self.partition, self.samplerType, name)

    def do(self, request):
        p = ipc.IPCPartition(self.partition)
        if not p.isValid():
            request.setResponseCode(404)
            request.write('<html>No such partition: %s</html>' % self.partition)
            request.finish()
            return
        try:
            samplers = get_samplers(self.partition,self.samplerType)
            request.setHeader('Content-Type','application/xml')
            request.write('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="/tdaq/web_is/xsl/default.xsl"?>\n')
            request.write('<samplers partition="%s">\n' % self.partition)
            request.write('\n'.join([construct_xml(s,'sampler') for s in samplers]))
            request.write('\n</samplers>\n')
            request.finish()
        except Exception as ex:
            request.setResponseCode(404)
            request.setHeader('Content-Type', 'text/plain')
            request.write(ex.message)
            request.finish()

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

class EMONSamplersResource(Resource):
    """
    Return the list of EMON samplers in a partition.
    This also implements the version where we show only samplers of a given type.
    """
    def __init__(self, partition):
        Resource.__init__(self)
        self.partition = partition

    def do(self, request):
        p = ipc.IPCPartition(self.partition)
        if not p.isValid():
            request.setResponseCode(404)
            request.write('<html>No such partition: %s</html>' % self.partition)
            request.finish()
            return
        try:
            samplers = get_samplers(self.partition)
            request.setHeader('Content-Type','application/xml')
            request.write('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="/tdaq/web_is/xsl/default.xsl"?>\n')
            request.write('<samplers partition="%s">\n' % self.partition)
            request.write('\n'.join([construct_xml(s,'sampler') for s in samplers]))
            request.write('\n</samplers>\n')
            request.finish()
        except Exception as ex:
            request.setResponseCode(404)
            request.setHeader('Content-Type', 'text/plain')
            request.write(ex.message)
            request.finish()

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

    def getChild(self, samplerType, request):
        if samplerType == '': return self
        return EMONSamplerTypeResource(self.partition, samplerType)

class EMONMonitorsResource(Resource):
    """
    Return the list of Event Monitors in a partition.
    """
    isLeaf = True

    def __init__(self, partition):
        Resource.__init__(self)
        self.partition = partition

    def do(self, request):
        p = ipc.IPCPartition(self.partition)

        if not p.isValid():
            request.setResponseCode(404)
            request.write('<html>No such partition: %s</html>' % self.partition)
            request.finish()
            return

        try:
            monitors = self.get_monitors()

            if 'json' in request.args.get('format', []):
                request.setHeader('content-type', 'application/json')
                request.write(json.dumps(monitors))
            else:
                request.setHeader('content-type', 'application/xml')
                string = '\n'.join(['<?xml version="1.0" encoding="UTF-8"?>',
                '<?xml-stylesheet type="text/xsl" href="/tdaq/web_is/xsl/default.xsl"?>',
                '<monitors release="current" partition="%s">',
                '%s',
                '</monitors>'])
                request.write(string % (self.partition, '\n'.join([construct_xml(m,'monitor') for m in monitors])))
            request.finish()
        except Exception as ex:
            request.setResponseCode(404)
            request.setHeader('Content-Type', 'text/plain')
            request.write(ex.message)
            request.finish()

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

    def get_monitors(self):
        """
        Return a list of dictionaries of monitors
        """
        p = ipc.IPCPartition(self.partition)
        stream = ISInfoStream(p, 'Monitoring', ISCriteria('/Monitor/.*',IS.MonitorInfo(p, '').type(),libispy.Logic.AND), False, 1)

        monitors = []

        while not stream.eof():
            name = stream.name()
            monitor = ISInfoDynAny()
            stream.get(monitor)

            monitor_dict =  dict(monitor)
            monitor_dict['time'] = bytes(monitor.time())
            monitor_dict['ctime'] = monitor.time().c_time()
            monitor_dict['name'] = name.replace("Monitoring./Monitor/","")
            monitor_dict['m_sampler_names'] = sorted(monitor_dict['m_sampler_names'])

            monitors.append(monitor_dict)

        return monitors
