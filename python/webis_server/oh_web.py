import os
import string
import urllib.request, urllib.parse, urllib.error
import tempfile
import re
import json
import logging

from twisted.internet import reactor, threads, task
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

from twisted.python.threadpool import ThreadPool

import ipc
from ispy import *
from oh import getRootObject, OHRootProvider
import ROOT

from .utils import *

logger = logging.getLogger(__name__)

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.SetStyle('Plain')
ROOT.gStyle.SetPalette(1)

pool = ThreadPool(10, 50)
pool.start()

class OHHistogramResource(Resource):

    isLeaf = True

    def __init__(self, partition, name, writable):
        Resource.__init__(self)
        self.partition = partition
        self.name      = name
        self.writable  = writable

    def do_POST(self, req, tag):
        try:
            histo = ROOT.TBufferJSON.ConvertFromJSON(req)
            (server, provider, arg) = self.name.split('.', 2)
            p = ipc.IPCPartition(self.partition)
            provider = OHRootProvider(p, server, provider, None)
            provider.publish(histo, arg, int(tag))
        except:
            logger.info("publish histo", exc_info=True);

    def render_POST(self, request):
        request.setHeader('content-type', 'application/json')
        if not self.writable:
            request.setResponseCode(403)
            return error_msg(PERMISSION_DENIED, 'Creating histograms not allowed')
        req = request.content.read()
        tag = request.args.get(b'tag', ["-1"])[0]
        pool.callInThread(self.do_POST, req, tag)
        request.setResponseCode(201)
        return b''

    def do_DELETE(self, request):
        try:
            d = ISInfoDictionary(ipc.IPCPartition(self.partition))
            d.remove(self.name)
            self.finish()
        except:
            logger.info('Delete histo %s failed', self.name, exc_info=True)
            request.setResponseCode(400)
            request.write(error_msg(OPERATION_FAILED, f'Delete failed for {self.name}'))
            request.finish()

    def render_DELETE(self, request):
        request.setHeader('content-type', 'application/json')
        if not self.writable:
            request.setResponseCode(403)
            return error_msg(PERMISSION_DENIED, 'Deleting histograms not allowed')
        reactor.callInThread(self.do_DELETE, request)
        return NOT_DONE_YET

    def render_GET(self, request):
        reactor.callInThread(self.do_GET, request)
        return NOT_DONE_YET

    def do_GET(self, request):
        use_json  = request.args.get(b'type',[b'png'])[0] in [ 'json', 'compact' ]
        p = ipc.IPCPartition(self.partition)
        if not p.isValid():
            reactor.callFromThread(write, request,
                                   error_msg(INVALID_PARTITION, f'No such partition: {self.partition}', use_json),
                                   finish=True,
                                   code=404,
                                   headers = use_json and { 'content-type' : 'application/json' } or None)
            return

        format  = request.args.get(b'type',[b'png'])[0].decode()
        draw    = request.args.get(b'draw',[b''])[0].decode()
        optstat = request.args.get(b'optstat',[None])[0]
        size    = request.args.get(b'size',[None])[0];
        if size: size = size.decode()
        logx    = request.args.get(b'logx', [None])[0]
        logy    = request.args.get(b'logy', [None])[0]
        logz    = request.args.get(b'logz', [None])[0]
        rangex  = request.args.get(b'range', [None])[0]
        if not rangex:
            rangex = request.args.get(b'rangex', [None])[0]
        rangey  = request.args.get(b'rangey', [None])[0]
        rangez  = request.args.get(b'rangez', [None])[0]
        rangeb  = request.args.get(b'rangebins', [None])[0]

        markercolor = request.args.get(b'markercolor', [None])[0]
        markerstyle = request.args.get(b'markerstyle', [None])[0]
        markersize = request.args.get(b'markersize', [None])[0]

        linecolor = request.args.get(b'linecolor', [None])[0]
        linestyle = request.args.get(b'linestyle', [None])[0]
        linewidth = request.args.get(b'linewidth', [None])[0]

        fillcolor = request.args.get(b'fillcolor', [None])[0]
        fillstyle = request.args.get(b'fillstyle', [None])[0]

        labelcolorx = request.args.get(b'labelcolorX', [None])[0]
        labelcolory = request.args.get(b'labelcolorY', [None])[0]
        labelcolorz = request.args.get(b'labelcolorZ', [None])[0]

        labelfontx = request.args.get(b'labelfontX', [None])[0]
        labelfonty = request.args.get(b'labelfontY', [None])[0]
        labelfontz = request.args.get(b'labelfontZ', [None])[0]

        labeloffsetx = request.args.get(b'labeloffsetX', [None])[0]
        labeloffsety = request.args.get(b'labeloffsetY', [None])[0]
        labeloffsetz = request.args.get(b'labeloffsetZ', [None])[0]

        labelsizex = request.args.get(b'labelsizeX', [None])[0]
        labelsizey = request.args.get(b'labelsizeY', [None])[0]
        labelsizez = request.args.get(b'labelsizeZ', [None])[0]

        title      = request.args.get(b'title',[None])[0]
        titlefont  = request.args.get(b'titlefont', [None])[0]
        titlesize  = request.args.get(b'titlesize',[None])[0]
        titleoffset = request.args.get(b'titleoffset',[None])[0]

        tag     = request.args.get(b'tag', [b'-1'])[0].decode()

        ref      = request.args.get(b'ref', [None])[0]
        refnorm  = request.args.get(b'refnorm', [None])[0]
        refcolor = request.args.get(b'refcolor', [None])[0]
        refstyle = request.args.get(b'refstyle', [None])[0]
        refwidth = request.args.get(b'refwidth', [None])[0]

        if rangex:
           minx, maxx = list(map(float, rangex.decode().split(',')))

        if rangey:
           miny, maxy = list(map(float, rangey.decode().split(',')))

        if rangez:
           minz, maxz = list(map(float, rangez.decode().split(',')))

        if rangeb:
           minx, maxx = list(map(int, rangeb.decode().split(',')))

        name = self.name
        histo = urllib.parse.unquote(name)

        try:
            (server, provider, arg) = histo.split('.',2)
            if arg.split('/')[-1].rfind('.') != -1:
                new_format = arg[arg.rfind('.') + 1:].decode()
                if new_format in [ 'png', 'jpeg', 'gif', 'root', 'xml', 'svg', 'ps', 'eps', 'pdf', 'json', 'compact' ]:
                    format = new_format
                    arg = arg[:arg.rfind('.')]
        except:
            (server, provider, arg) = ('','','')

        fname = tempfile.mktemp(suffix='.'+format)

        info_exists = False
        try:
            info_dir = ISInfoDictionary(p)
            info_exists = info_dir.contains('.'.join([server,provider,arg]))
        except:
            info_exists = False

        if not info_exists:
            if size:
                w, h = size.split('x')
                # c = ROOT.TCanvas("oh","oh", int(w), int(h))
                c = ROOT.TCanvas(False)
                c.SetCanvasSize(int(w),int(h))
            else:
                c = ROOT.TCanvas(False)
            h = ROOT.TH1F(name,name, 1,0.0,1.0)
            ROOT.gStyle.SetOptStat('')
            h.Draw(draw)
            t = ROOT.TPaveText(0.1, 0.3, 0.9, 0.7);
            txt = t.AddText("Histogram does not exist");
            txt.SetTextAngle(12)
            t.Draw();

            ROOT.gPad.Update();

            if format == 'json':
                result = bytes(ROOT.TBufferJSON.ConvertToJSON(c,3).Data(),'utf-8')
            elif format == 'compact':
                result = bytes(ROOT.TBufferJSON.ConvertToJSON(h,3).Data(),'utf-8')
            else:
                c.SaveAs(fname)

            del h
            del c

        elif format == 'root' or format == 'xml':
            h = getRootObject(p, server, provider, arg, int(tag))
            ROOT.SetOwnership(h,True)
            f = ROOT.TFile.Open(fname, 'RECREATE')
            paths = h.GetName().split('/')
            (s, p, r ) = paths[0].split('.')
            d = f.mkdir(s).mkdir(p)

            for p in paths[1:-1]:
                d = d.mkdir(p)
            d.cd()
            h1 = h.Clone(paths[-1])
            h1.Write()
            f.Close()
            del h
            del h1
        else:

            if size:
                w, h = size.split('x')
                # c = ROOT.TCanvas("oh","oh", int(w), int(h))
                c = ROOT.TCanvas()
                c.SetCanvasSize(int(w),int(h))
            else:
                c = ROOT.TCanvas()

            pad = c.cd(1)

            h = getRootObject(p, server, provider, arg)
            ROOT.SetOwnership(h,True)

            if logx:
                pad.SetLogx(int(logx.decode()))

            if logy:
                if h.GetEntries() == 0.0:
                    h.SetMaximum(1.0)
                pad.SetLogy(int(logy.decode()))

            if logz:
                pad.SetLogz(int(logz.decode()))

            ROOT.gStyle.SetOptStat(1);
            if optstat:
                optstat = optstat.decode()
                if re.match('[012]+', optstat):
                    ROOT.gStyle.SetOptStat(int(optstat))
                else:
                    ROOT.gStyle.SetOptStat(optstat)
            else:
                ROOT.gStyle.SetOptStat('emr')

            h.UseCurrentStyle()

            if rangex:
                h.GetXaxis().SetRangeUser(minx, maxx)
            elif rangeb:
                h.GetXaxis().SetRange(minx, maxx)

            if rangey:
                h.GetYaxis().SetRangeUser(miny, maxy)

            if rangez:
                h.GetZaxis().SetRangeUser(minz, maxz)

            if markercolor:
                h.SetMarkerColor(int(markercolor.decode()))

            if markersize:
                h.SetMarkerSize(int(markersize.decode()))

            if markerstyle:
                h.SetMarkerStyle(int(markerstyle.decode()))

            if linecolor:
                h.SetLineColor(int(linecolor.decode()))

            if linestyle:
                h.SetLineStyle(int(linestyle.decode()))

            if linewidth:
                h.SetLineWidth(int(linewidth.decode()))

            if fillcolor:
                h.SetFillColor(int(fillcolor.decode()))

            if fillstyle:
                h.SetFillStyle(int(fillstyle))

            if labelcolorx:
                h.SetLabelColor(int(labelcolorx), 'X')

            if labelcolory:
                h.SetLabelColor(int(labelcolory), 'Y')

            if labelcolorz:
                h.SetLabelColor(int(labelcolorz), 'Z')

            if labelfontx:
                h.SetLabelFont(int(labelfontx), 'X')

            if labelfonty:
                h.SetLabelFont(int(labelfonty), 'Y')

            if labelfontz:
                h.SetLabelFont(int(labelfontz), 'Z')

            if labeloffsetx:
                h.SetLabelOffset(float(labeloffsetx), 'X')

            if labeloffsety:
                h.SetLabelOffset(float(labeloffsety), 'Y')

            if labeloffsetz:
                h.SetLabelOffset(float(labeloffsetz), 'Z')

            if labelsizex:
                h.SetLabelSize(float(labelsizex), 'X')

            if labelsizey:
                h.SetLabelSize(float(labelsizey), 'Y')

            if labelsizex:
                h.SetLabelSize(float(labelsizez), 'Z')

            if title:
                h.SetTitle(title)

            if titlefont:
                h.SetTitleFont(int(titlefont),'')

            if titlesize:
                h.SetTitleSize(int(titlesize),'')

            if titleoffset:
                h.SetTitleOffset(int(titleoffset),'')

            if h.ClassName().startswith('TGraph') and draw == '':
                draw = 'AL'
            if draw == '' and h.ClassName().startswith('TH2'):
                draw = 'colz'
            h.Draw(draw)

            ROOT.gPad.Update();

            if ref:
                rserver, rprovider, rrest = ref.split('.')
                if info_dir.contains('.'.join([rserver,rprovider,rrest])):
                    href = getRootObject(p,rserver, rprovider, rrest)
                    ROOT.SetOwnership(href,True)

                    if refcolor:
                        href.SetLineColor(int(refcolor.decode()))
                    else:
                        href.SetLineColor(ROOT.kRed)
                    if refstyle:
                        href.SetLineStyle(int(refstyle.decode()))
                    if refwidth:
                        href.SetLineWidth(int(refwidth.decode()))
                    if refnorm != '0':
                        href.Scale(old_div(h.GetEntries(),href.GetEntries()))
                    href.Draw('SAME')

            if format == 'json':
                result = bytes(ROOT.TBufferJSON.ConvertToJSON(c,3).Data(),'utf-8')
            elif format == 'compact':
                result = bytes(ROOT.TBufferJSON.ConvertToJSON(h,3).Data(),'utf-8')
            else:
                c.SaveAs(fname)

            del c

        request.setHeader('cache-control','no-cache, must-revalidate')

        if format == 'png':
            content = 'image/png'
        elif format == 'gif':
            content = 'image/gif'
        elif format == 'jpeg':
            content = 'image/jpeg'
        elif format == 'svg':
            content = 'image/svg+xml'
        elif format == 'ps':
            content = 'application/postscript'
            request.setHeader('Content-disposition', 'attachment; filename=OHOutput.ps')
        elif format == 'eps':
            content = 'application/postscript'
            request.setHeader('Content-disposition', 'attachment; filename=OHOutput.eps')
        elif format == 'pdf':
            content = 'application/pdf'
            request.setHeader('Content-disposition', 'attachment; filename=OHOutput.pdf')
        elif format == 'xml':
            content = 'application/xml'
        elif format == 'json' or format == 'compact':
            content = 'application/json'
        else:
            content = 'application/octetstream'
            request.setHeader('Content-disposition', 'attachment; filename=OHOutput.root')

        request.setHeader('Content-type', content)

        # print fname

        if format not in [ 'json', 'compact' ]:
            try:
              with open(fname, 'rb') as f:
                result = f.read()
              os.remove(fname)
            except Exception as ex:
              result = b'';

        reactor.callFromThread(write, request, bytes(result), finish=True)

class OHProvidersResource(Resource):

    def __init__(self, partition, writable):
        Resource.__init__(self)
        self.partition = partition
        self.writable = writable

    def render_GET(self, request):
        use_json = b'json' in request.args.get(b'format',[])

        p = ipc.IPCPartition(self.partition)
        if not p.isValid():
            request.setResponseCode(404)
            return error_msg(INVALID_PARTITION, f'No such partition: {self.partition}', use_json,
                             headers = use_json and { 'content-type': 'application/json'} or None)

        srv = ISServerIterator(p)

        providers = []
        while srv.next():
            server = srv.name()
            it = ISInfoIterator(p, server, ISCriteria(ISInfoDynAny(p, 'HistogramProvider').type()))
            while it.next():
                name = it.name().split('.',1)[1]
                providers.append((name, server))

        providers.sort(key=lambda x: x[0].lower())

        if use_json:
            request.setHeader('Content-type','application/json')
            result = json.dumps([ { 'provider': t[0], 'server': t[1] } for t in providers ])
        else:
            request.setHeader('Content-type','application/xml')

            result = """<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="../../../tdaq/web_is/xsl/default.xsl"?>\n<providers release="current" partition="%s">\n""" % self.partition
            result += '\n'.join([ '<provider name="%s" server="%s"/>' % (name, server) for (name, server) in providers ])
            result += '\n</providers>'

        return bytes(result,'utf-8')

    def getChild(self, name, request):
        if name == b'': return self
        name = name.decode()
        if request.postpath:
            name += '/' + '/'.join([ p.decode() for p in request.postpath])
        return OHHistogramResource(self.partition, name, self.writable)
