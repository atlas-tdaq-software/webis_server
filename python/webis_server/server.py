#!/usr/bin/env tdaq_python

import json
import logging

from twisted.web.server import Site
from twisted.web import static
from twisted.web.resource import Resource
from twisted.internet import reactor
from twisted.internet.endpoints import serverFromString

import ipc
from ispy import *

from .is_web      import ISServersResource
from .oh_web      import OHProvidersResource
from .oks_web     import OKSClassResource
from .ers_web     import ERSResource
from .emon_events import EMONResource
from .utils       import error_msg, INVALID_PARTITION

class ServicesResource(Resource):

    def __init__(self, partition, writable):
        Resource.__init__(self)
        self.partition = partition
        self.writable  = writable

    def render_GET(self, request):

        use_json = b'json' in request.args.get(b'format', [])
        if use_json:
            request.setHeader('Content-type','application/json')

        p = ipc.IPCPartition(self.partition)
        if not p.isValid():
            request.setResponseCode(404)
            return error_msg(INVALID_PARTITION, f'No such partition: {self.partition}', use_json)

        if use_json:
            return bytes(json.dumps([
                    { 'name':'is', 'descr': 'Information Service' },
                    { 'name': 'oh', 'descr': 'Histogram Service'},
                    { 'name': 'oks', 'descr': 'OKS Service'},
                    # { 'name': 'segment', 'descr': 'Run control segments'},
                    # { 'name': 'emon', 'descr':'Emon Service'},
                    { 'name' : 'event', 'descr': 'EMON Event Server'},
                    { 'name': 'ers', 'descr' : 'ERS Service'}
                    ]),'utf-8')
        else:
            request.setHeader('content-type', 'application/xml')
            return bytes("""<?xml version="1.0" encoding="UTF-8"?>
  <?xml-stylesheet type="text/xsl" href="../../tdaq/web_is/xsl/default.xsl"?>
  <services release="%s" partition="%s">
  <service name="is" descr="Information Service"/>
  <service name="oh" descr="Histogram Service"/>
  <service name="oks" descr="OKS Service"/>
  <service name="event" descr="EMON Event Service"/>
  <service name="ers" descr="ERS Service"/>
</services>
""" % ( 'current', self.partition), 'utf-8')

    def getChild(self, name, request):
        if name == b'': return self
        if name == b'is':
            return ISServersResource(self.partition, self.writable)
        elif name == b'oh':
            return OHProvidersResource(self.partition, self.writable)
        elif name == b'oks':
            return OKSClassResource(self.partition)
        elif name == b'event':
            return EMONResource(self.partition)
        elif name == b'ers':
            return ERSResource(self.partition, self.writable)

class PartitionsResource(Resource):

    def __init__(self, writable):
        Resource.__init__(self)
        self.writable = writable

    def render_GET(self, request):
        partitions = [ p.name() for p in ipc.getPartitions() ]
        partitions.append('initial')
        partitions.sort(key=lambda x: x.lower())

        origin = request.getHeader('origin')
        if origin and origin.endswith('.cern.ch'):
            request.setHeader('Access-Control-Max-Age','600')
            request.setHeader('Access-Control-Allow-Origin', origin)
            request.setHeader('Access-Control-Allow-Methods', 'GET')
            request.setHeader('Access-Control-Allow-Credentials', 'true')
            request.setHeader('Access-Control-Allow-Headers','x-prototype-version,x-requested-with')

        if b'json' in request.args.get(b'format',[]):
            request.setHeader('Content-type','application/json')
            return bytes(json.dumps(partitions),'utf-8')
        else:
            request.setHeader("Content-type", "application/xml")
            return bytes("""<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../tdaq/web_is/xsl/default.xsl"?>
<partitions release="current">
%s
</partitions>
""" % ( '\n'.join([ '<partition name="' + p + '"/>' for p in partitions ]) ),'utf-8')

    def getChild(self, name, request):
        origin = request.getHeader('origin')
        if origin and origin.endswith('.cern.ch'):
            request.setHeader('Access-Control-Max-Age','600')
            request.setHeader('Access-Control-Allow-Origin', origin)
            request.setHeader('Access-Control-Allow-Methods', 'GET')
            request.setHeader('Access-Control-Allow-Credentials', 'true')
            request.setHeader('Access-Control-Allow-Headers','x-prototype-version,x-requested-with')

        if name == b'': return self
        return ServicesResource(name.decode(), self.writable)

def generate_certificate():

    from OpenSSL  import crypto
    from socket   import getfqdn
    from tempfile import NamedTemporaryFile

    # issuer and subject = fully qualified host name
    cert = crypto.X509()
    cert.get_subject().CN = getfqdn()
    cert.set_issuer(cert.get_subject())

    # validity 1 year
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(3600 * 24 * 365)

    # 2048 bit RSA key
    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 2048)
    cert.set_pubkey(key)

    # sign it
    cert.sign(key, 'sha256')

    cert_file = NamedTemporaryFile(mode="wt", delete=False)
    cert_file.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('utf-8'))

    key_file = NamedTemporaryFile(mode="wt", delete=False)
    key_file.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key).decode('utf-8'))

    return ':certKey=' + cert_file.name, ':privateKey=' + key_file.name

if __name__ == '__main__':

    import argparse
    import os

    parser = argparse.ArgumentParser(description="WebIS server")
    parser.add_argument('-p', '--port', help="Listen on port number", type=int, default=8080)
    parser.add_argument('-P', '--port-file', help="File to write port number to")
    parser.add_argument('-u', '--unix', help="Listen on Unix socket")
    parser.add_argument('-w', '--www', help='tdaq/web_is static directory', default='www')
    parser.add_argument('-l', '--loglevel', help='log level', default='WARNING')
    parser.add_argument('-c', '--cert', help='TLS certificate', default='', type=lambda n : n != '' and f':certKey={n}' or '')
    parser.add_argument('-k', '--key', help='TLS key', default='', type=lambda n : n != '' and f':privateKey={n}' or '')
    parser.add_argument('-s', '--self-signed', help='Generate self-signed certificate internally', action='store_true')
    parser.add_argument('-W', '--writable', help='Allow write (POST) operations in IS', action='store_true')
    args = parser.parse_args()

    logging.basicConfig(level = getattr(logging, args.loglevel))

    root = Resource()
    info = Resource()
    root.putChild(b'info', info)
    info.putChild(b'current', PartitionsResource(args.writable))

    tdaq = Resource()
    tdaq.putChild(b'web_is',static.File(args.www))
    root.putChild(b'tdaq', tdaq)

    reactor.suggestThreadPoolSize(50)

    if args.self_signed:
        args.cert, args.key = generate_certificate()

    if args.unix:
        s = f'unix:{args.unix}'
    elif args.cert != '' or args.key != '':
        s = f'ssl:{args.port}{args.cert}{args.key}:interface=\:\:'
    else:
        s = f'tcp:{args.port}:interface=\:\:'
    endpoint = serverFromString(reactor, s)

    if args.self_signed:
        os.remove(args.cert[len(':certKey='):])
        os.remove(args.key[len(':privateKey:'):])
    iport = endpoint.listen(Site(root))
    if args.port_file:
      with open(args.port_file, "w") as f:
        iport.addCallback(lambda p: print(p.getHost().port, file=f, flush=True))
    reactor.threadpool.max = 50
    reactor.run()
