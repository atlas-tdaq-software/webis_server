from builtins import bytes
from builtins import range
import logging
import string
import cgi
import html
import json
import config

from twisted.internet import reactor, threads
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

import ipc
from ispy import *
from .is_format import *

def segment_controller(segment_name, controller, default_host, root_controller):
    """
    Return the entry for a segment controller.
    """
    if hasattr(controller, 'RunsOn') and controller.RunsOn:
        controller_name = controller.id
        runsOn = controller.RunsOn and controller.RunsOn.id or default_host.id
    elif default_host:
        controller_name = segment_name + ':' + default_host.id.split('.')[0]
        runsOn = default_host.id
    else:
        controller_name = segment_name + ':' + 'unkown.cern.ch'
        runsOn = 'unknown'

    if segment_name == b'':
        segment_name = root_controller
    return { 'name': segment_name, 'ctrl': controller_name,  'host': runsOn, 'app': controller.id }

def isRunControlled(app):
    "Is this a run controlled application ?"
    return hasattr(app, 'ActionTimeout')

def expand_template_applications(applications, segment_name, template_hosts):
    """
    Expand all template applications. At the moment this does not work
    correctly for the L2PUs since there is more magic involved.
    """
    result = []
    for h in template_hosts:    
        for a in applications:
            instances = a.Instances
            if instances == 0:
                instances = h.NumberOfCores
            for i in range(1,instances + 1):
                r = { 'name' : a.id + ':' + segment_name + ':' + h.id.split('.')[0] + ':' + bytes(i), 'host': h.id }
                if isRunControlled(a):
                    r['ctrl'] = r['name']
                result.append(r)

    return result

def expand_resource(r, default_host):
    """
    Expand resource recursively if necessary.
    """
    result = []
    if hasattr(r, 'RunsOn'):
        res = { 'name' : r.id, 'host': r.RunsOn and r.RunsOn.id or default_host.id }
        if isRunControlled(r):
            res['ctrl'] = res['name']
        result.append(res)
        
    if hasattr(r, 'Contains'):
        for x in r.Contains:
            result.extend(expand_resource(x, default_host))

    return result

def make_application(app, default_host):
    r = { 'name' : app.id, 'host': app.RunsOn and app.RunsOn.id or default_host.id }
    if isRunControlled(app):
        r['ctrl'] = r['name']
    return r

def make_segment(partition_name, segment_name, controller, default_host,
                 sub_segments, applications, template_applications,  template_hosts,
                 resources, disabled, infra, ctrl_default_host):
    """
    Get all information for one segment.
    """
    root_controller = 'RootController' 
    if partition_name == 'initial':
        root_controller = 'DefaultRootController'

    result = {
        'partition' : partition_name,
        'segment'   : [ segment_controller(segment_name, controller, ctrl_default_host, root_controller) ]
        }

    result['applications'] = [ make_application(a, default_host) for a in applications ]
    result['template_applications'] = expand_template_applications(template_applications, segment_name,  template_hosts)
    result['resources'] =  sum([ expand_resource(r, default_host) for r in resources if r.id not in disabled ],[])
    result['subsegments'] = [ segment_controller(a.id, a.IsControlledBy, a.DefaultHost and a.DefaultHost or default_host, root_controller) for a in sub_segments if a.id not in disabled ]
    result['infrastructure'] = [ { 'name' : hasattr(a,'RunsOn') and a.id or a.id + ':' + segment_name + ':' + default_host.id.split('.')[0], 'host' : hasattr(a,'RunsOn') and a.RunsOn and a.RunsOn.id or default_host.id } for a in infra ]

    return result;

class SegmentResource(Resource):

    def __init__(self, partition, segment):
        Resource.__init__(self)
        self.partition = partition
        self.segment = segment

    def do(self, request):
        p = IPCPartition(self.partition)
        if not p.isValid():
            raise RuntimeError('No such partition: %s' % self.partition)
        
        if self.partition == 'initial':
            db = config.Configuration('rdbconfig:RDB_INITIAL@initial')
        else:
            db = config.Configuration('rdbconfig:RDB@' + self.partition)

        partition = db.get_dal('Partition', self.partition)
        segment = db.get_dal('Segment', self.segment)
        sub_segments = segment.Segments
        default_host = segment.DefaultHost and segment.DefaultHost or partition.DefaultHost
        controller =   segment.IsControlledBy
        applications = segment.Applications
        resources    = segment.Resources
        template_applications = []
        template_hosts        = []
        try:
            template_applications = segment.TemplateApplications
            template_hosts        = segment.TemplateHosts
        except:
            pass
        ctrl_default_host = segment.DefaultHost and segment.DefaultHost or partition.DefaultHost
        infrastructure    = segment.Infrastructure

        r = make_segment(self.partition, self.segment, controller, default_host, sub_segments, applications, template_applications, template_hosts, resources, [ s.id for s in partition.Disabled], infrastructure, ctrl_default_host)

        request.setHeader("Content-Type", "application/json")
        request.write(json.dumps(r))
        request.finish()

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET


class TopSegmentResource(Resource):
    """
    Top level interface, opens data base and handles special case
    of top segment == partition.
    """

    def __init__(self, partition):
        Resource.__init__(self)
        self.partition = partition

    def do(self, request):
        p = IPCPartition(self.partition)
        if not p.isValid():
            raise RuntimeError('No such partition: %s' % self.partition)
        
        if self.partition == 'initial':
            db = config.Configuration('rdbconfig:RDB_INITIAL@initial')
        else:
            db = config.Configuration('rdbconfig:RDB@' + self.partition)

        partition = db.get_dal('Partition', self.partition)

        segment = None
        sub_segments = partition.Segments
        controller   = partition.OnlineInfrastructure.IsControlledBy
        default_host = partition.DefaultHost
        applications = []
        resources    = []
        template_applications = []
        template_hosts        = []
        infrastructure = partition.OnlineInfrastructure.Infrastructure
        # ctrl_default_host = partition.OnlineInfrastructure.DefaultHost and  partition.OnlineInfrastructure.DefaultHost or partition.DefaultHost
        ctrl_default_host = partition.DefaultHost

        r = make_segment(self.partition, '', controller, default_host, sub_segments, applications, template_applications, template_hosts, resources, [ s.id for s in partition.Disabled], infrastructure, ctrl_default_host)

        request.setHeader("Content-Type", "application/json")
        request.write(json.dumps(r))
        request.finish()

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

    def getChild(self, segment, request):
        if segment == '': return self
        return SegmentResource(self.partition, segment)
        

