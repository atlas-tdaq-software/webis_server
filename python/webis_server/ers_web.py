import logging
import string
import cgi
import html
import json
import urllib.parse

from twisted.internet import reactor, threads, task
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

import ers
import ipc
import mts

logger = logging.getLogger(__name__)

subscriptions = {}

def get_subscription(partition, subscription):
    """
    Either return an existing subscription that matches exactly.
    Or create a new MTS subscription.
    """
    key = ':'.join([partition, subscription])
    s = subscriptions.get(key)
    if not s:
        logger.info('Creating MTSSubscription: %s %s', partition, subscription)
        s = MTSSubscription(partition, subscription)
        subscriptions[key] = s

    return s

class MTSSubscription:
    """
    The MTS callback handler.
    It distributes the ERS message to all connections subscribed to the same expression
    """

    def __init__(self, partition, subscription):
        self.partition = partition
        self.subscription = subscription
        self.key = ':'.join([self.partition, self.subscription])
        self.connections = []
        self.sub = None
        self.keep_alive = task.LoopingCall(self.ping)
        self.keep_alive.start(15)

    def add_connection(self, connection):
        """
        Add another listener with the same subscription criteria.
        """
        if len(self.connections) == 0:
            logger.info('Subscribing to partition %s with %s', self.partition, self.subscription)
            self.sub = mts.add_receiver(self.handler, self.partition, self.subscription)

        self.connections.append(connection)
        deferred = connection.notifyFinish()
        deferred.addCallback(lambda reason: self.remove_connection(connection))
        deferred.addErrback(lambda reason: self.remove_connection(connection))

    def remove_connection(self, connection):
        """
        Remove a listener, typically if the client closed the connection, or the partition goes down.
        """
        logger.info("Client %s closed connection", connection.getClientAddress())
        self.connections.remove(connection)
        if len(self.connections) == 0:
            logger.info('No clients left, unsubscribing from partition %s with %s', self.partition, self.subscription)
            self.sub.remove_receiver()
            del subscriptions[self.key]

    def ping(self):
        self.send(b':keep-alive\n\n')

    def handler(self, issue):
        """
        Callback to handle a single ERS issue: send the information to all listeners.
        """
        msg = bytes(f'event: issue\ndata: {json.dumps(issue)}\n\n', 'utf-8')
        self.send(msg)

    def send(self, msg):
        for c in self.connections:
            try:
                c.write(msg)
            except:
                logger.error('Write failed: removing listener from partition %s with %s', self.partition, self.subscription, exc_info=True)
                self.remove_connection(c)
        if len(self.connections) == 0:
            self.sub.remove_receiver()
            self.keep_alive.stop()
            del subscriptions[self.key]

class ERSSubscriptionResource(Resource):
    """
    An ERS Subscription
    """
    def __init__(self, partition, subscription):
        Resource.__init__(self)
        self.partition    = partition
        self.subscription = subscription

    def render_GET(self, request):
        threads.deferToThread(self.do, request)
        return NOT_DONE_YET

    def do(self, request):
        p = ipc.IPCPartition(self.partition)

        if not p.isValid():
            request.setResponseCode(404)
            request.write(bytes('<html>No such partition: %s</html>' % self.partition,'utf-8'))
            request.finish()
            return

        logger.info('New client connected: %s', request.getClientAddress())
        sub = get_subscription(self.partition, self.subscription)
        try:
            sub.add_connection(request)
            request.setHeader('content-type', 'text/event-stream')
            request.setHeader('connection','keep-alive')
            request.setHeader('cache-Control','no-cache, must-revalidate')
            request.write(b': subscribed\n\n')
        except:
            request.finish()
            return

        return NOT_DONE_YET

class ERSResource(Resource):
    """
    """

    def __init__(self, partition, writable):
        Resource.__init__(self)
        self.partition = partition
        self.writable = writable

    def render_GET(self, request):
        request.setResponseCode(400)
        return b'<html>Specify a subscription</html>'

    def render_POST(self, request):
        if not self.writable:
            request.setResponseCode(403)
            return b'<html>Not allowd</html>'

        try:
            req = json.load(request.content)
            pctx = mts.RemoteProcessContext(req.get('host', 'no-host'),
                                            req.get('pid', -1),
                                            req.get('tid', -1),
                                            req.get('cwd', 'no-cwd'),
                                            req.get('uid', -1),
                                            req.get('user', 'no-user'),
                                            req.get('app', 'no-app'))

            ctx = mts.RemoteContext(req.get('pkg', 'no-package'),
                                    req.get('file', 'no-file'),
                                    req.get('line', -1),
                                    req.get('func', 'no-function'),
                                    pctx)

            issue = mts.make_issue(ctx,
                                   req.get('msg', 'no-msgid'),
                                   req.get('sev', 'LOG'),

                                   req.get('time', ipc.OWLTime().str()),
                                   req.get('message', ''),
                                   req.get('qual', []),
                                   req.get('param', {}))
            mts.send_issue(issue)

            request.setResponseCode(201)
        except:
            logger.info("Post ers issue", exc_info=True)
            request.setResponseCode(400)
        return b''

    def getChild(self, subscription, request):
        if subscription == b'':
            return self
        print(f'Subscription: {urllib.parse.unquote(subscription.decode())}')
        return ERSSubscriptionResource(self.partition, urllib.parse.unquote(subscription.decode()))
